#include <catch2/catch_test_macros.hpp>
#include "aidge/graphRegex/GraphStrInterpreter.hpp"


using namespace Aidge;
TEST_CASE("GraphStrInterpreter") {



    std::vector<std::string> tests = {
           

            //sequ
            "A;",
            "A->B",
            "A->B->C",
            //seq and common
            "A#",
            "A#->B",
            "A#->B#",
            "A#->B#->C",
            "A#->B#->C#",
            "A->B#->C",
            //sequ quantif +
            "A+",
            "A+->B+",
            "A->B+->C",
            //sequ quantif *
            "A*",
            "A*->B*",
            "A->B*->C",

            //sequ quantif 
            "A*",
            "A*->B+",
            "A+->B*->C",
            //others

            "(A#->B->C#)+",
            "(A#->B)+;A#->B->C",
            "B+->B->B",
            "B#->R*",
            "(B#->R)*",
            "A->C->B#->B;B#->R",
            "B#->R",
            "A->C#;A->C#;A->C#;A->C#;A->C#;A->C#",
            "B#->R;B#->R",
            "A# -> C -> B#; B#->A#",
            
        // Add more test cases here
    };

    SECTION("AST Regex bijection") {

        for (const std::string& test : tests) {
            std::shared_ptr<GraphStrInterpreter>  strGenerator = std::make_shared<GraphStrInterpreter>(test);
            std::string astString = strGenerator->interpret();
            //supress space in the test becase erase in the AST
            std::string testNoS = test;
            testNoS.erase(std::remove_if(testNoS.begin(), testNoS.end(), ::isspace), testNoS.end());
            //if the last char is ; (SEP) it will not in the AST and it's not a bug erase it
            if (!testNoS.empty() && testNoS.back() == ';') {
                // Remove the last character
                testNoS.pop_back();
            }
            //test
            REQUIRE(astString == testNoS);
        }

    }
}