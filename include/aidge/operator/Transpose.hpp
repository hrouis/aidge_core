/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_TRANSPOSE_H_
#define AIDGE_CORE_OPERATOR_TRANSPOSE_H_

#include <array>
#include <cmath>
#include <numeric>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
class Transpose_OpImpl : public OperatorImpl {
public:
    Transpose_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}
    void forward() override;
};

enum class TransposeAttr { OutputDimsOrder };

class Transpose_Op : public OperatorTensor,
                public Registrable<Transpose_Op, std::string, std::shared_ptr<OperatorImpl>(const Transpose_Op&)>,
                public StaticAttributes<TransposeAttr, std::vector<DimSize_t>> {

   public:
    static const std::string Type;

    Transpose_Op() = delete;

    using Attributes_ = StaticAttributes<TransposeAttr, std::vector<DimSize_t>>;
    template <TransposeAttr e>
    using attr = typename Attributes_::template attr<e>;

    Transpose_Op(const std::vector<DimSize_t> &output_dims_order)
        : OperatorTensor(Type, 1, 0, 1),
          Attributes_(attr<TransposeAttr::OutputDimsOrder>(output_dims_order))
    {
        mImpl = std::make_shared<Transpose_OpImpl>(*this);
    }

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    Transpose_Op(const Transpose_Op& op)
        : OperatorTensor(op),
          Attributes_(op)
    {
        if (!op.backend().empty()) {
            SET_IMPL_MACRO(Transpose_Op, *this, op.backend());
        }
        else {
            mImpl = std::make_shared<Transpose_OpImpl>(*this);
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Transpose_Op
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Transpose_Op>(*this);
    }

    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

inline std::shared_ptr<Node> Transpose(const std::vector<DimSize_t> &output_dims_order,
                                           const std::string& name = "") {
    return std::make_shared<Node>(std::make_shared<Transpose_Op>(output_dims_order), name);
}
}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::TransposeAttr>::data[] = {"OutputDimsOrder"};
}

#endif /* AIDGE_CORE_OPERATOR_TRANSPOSE_H_ */
