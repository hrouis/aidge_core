
#include <catch2/catch_test_macros.hpp>
#include "aidge/graphRegex/GraphParser.hpp"
#include "aidge/utilsParsing/AstNode.hpp"
#include <iostream>


using namespace Aidge;

    //generative function ,
    std::string domain();
    std::string exp() {
        int randomValue = std::rand() % 3;
        switch (randomValue) {
        case 0:
            return "A";
        case 1 :
            return "A#";
        default:
             return domain();

        }
    }

    std::string seq() {
        int randomValue = std::rand() % 2;
        switch (randomValue) {
        case 0:
            return exp();
        default:
             return  exp()+"->"+seq();
        }
    }

    std::string domain() {
        int randomValue = std::rand() % 2;

        switch (randomValue) {
            // case 0:
            //     return  seq();
            // case 1:
            //     return  seq() + "->" +domain();

            case 0:
                return  "("+ seq() +")*";
            default:
                return  "("+ seq() +")+";

            // case 4:
            //     return  "("+ domain() +")*" + "->" +domain();
            // default:
            //     return  "("+ domain() +")+" + "->" +domain();
        }
    }

    std::string allExpr() {
        int randomValue = std::rand() % 2;
        switch (randomValue) {
            case 0:
                return  seq();
            default :
                return  seq()+ ";" +allExpr();
        }
    }

/*
exp : KEY(QOM | QZM)?  | CKEY | domain
seq :exp (NEXT seq)* 
domain :  LPAREN seq RPAREN (QOM | QZM) 
allExpr: seq (SEP allExpr)*
*/
TEST_CASE("GraphParser", "Test_GraphParser") {

    SECTION("Empty") {
        for (int i = 0; i < 100; ++i) {
            const std::string test = allExpr();
            std::cout << test <<"\n";
            GraphParser graphParser = GraphParser(test);
            std::shared_ptr<AstNode<gRegexTokenTypes>> tree = graphParser.parse();
        }
    }
}