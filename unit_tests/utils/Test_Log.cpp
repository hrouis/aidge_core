/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/utils/Log.hpp"

#include <fmt/color.h>

using namespace Aidge;

TEST_CASE("[core/log] Log") {
    SECTION("TestLog") {
        Log::setConsoleLevel(Log::Debug);
        Log::debug("debug");
        Log::debug("{}", fmt::styled("green debug", fmt::fg(fmt::color::green)));
        Log::info("info");
        Log::notice("notice");
        Log::warn("warn");
        Log::error("error");
        Log::fatal("fatal");
    }
}
