#include <catch2/catch_test_macros.hpp>
#include "aidge/nodeTester/ConditionalLexer.hpp"
#include "aidge/utilsParsing/ParsingToken.hpp"

#include <iostream>
#include <map>
#include <functional>

using namespace Aidge;

TEST_CASE("nodeTester", "Lexer") {
    SECTION("RandomGenerateTest") {

        std::map<ConditionalTokenTypes, std::function<std::pair<std::string, std::string>()>> LexerTestMap{
            {ConditionalTokenTypes::AND,      +[](){return std::pair<std::string, std::string>("&& ","");}},
            {ConditionalTokenTypes::OR,       +[](){return std::pair<std::string, std::string>("|| ","");}},
            {ConditionalTokenTypes::EQ,       +[](){return std::pair<std::string, std::string>("== ","");}},
            {ConditionalTokenTypes::NEQ,      +[](){return std::pair<std::string, std::string>("!= ","");}},

            {ConditionalTokenTypes::KEY,      +[](){return std::pair<std::string, std::string>("A ","A");}},

            {ConditionalTokenTypes::BOOL,     +[](){
                std::size_t keyLen = (std::rand() % 2);
                const std::vector<std::string> characters = {"true","false"};
          
                return std::pair<std::string, std::string>(characters[keyLen]+" ",characters[keyLen]);}
            },

            {ConditionalTokenTypes::INTEGER,  +[](){
                std::size_t keyLen = (std::rand() % 20)+1;
                const std::string characters = "1234567890";
                std::size_t randomIndex = std::rand() % characters.size();
                std::string key;
                for (std::size_t i = 0; i < keyLen; ++i) {
                    key += characters[randomIndex];
                    randomIndex = std::rand() % characters.size();
                }
                return std::pair<std::string, std::string>(key+" ",key);}
            },

            {ConditionalTokenTypes::FLOAT,    +[](){
                std::size_t keyLen = (std::rand() % 20)+2;
                const std::string characters = "1234567890";
                std::size_t randomIndex = std::rand() % characters.size();
                std::string key;
                for (std::size_t i = 0; i < keyLen/2; ++i) {
                    key += characters[randomIndex];
                    randomIndex = std::rand() % characters.size();
                }
                key += ".";
                for (std::size_t i = 0; i < keyLen/2; ++i) {
                    key += characters[randomIndex];
                    randomIndex = std::rand() % characters.size();
                }
                return std::pair<std::string, std::string>(key+" ",key);}
            },

            {ConditionalTokenTypes::STRING,   +[](){
                std::size_t keyLen = (std::rand() % 20)+1;
                const std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ";
                std::size_t randomIndex = std::rand() % characters.size();
                std::string key;
                for (std::size_t i = 0; i < keyLen; ++i) {
                    key += characters[randomIndex];
                    randomIndex = std::rand() % characters.size();
                }

                return std::pair<std::string, std::string>("'"+key+"' ",key);}
            },

            {ConditionalTokenTypes::LAMBDA,   +[](){

                std::size_t keyLen = (std::rand() % 20)+1;
                const std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                const std::string Startchar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

                std::size_t randomIndex = std::rand() % characters.size();
                std::size_t randomStartIndex = std::rand() % Startchar.size();

                std::string key;
                key += Startchar[randomStartIndex];

                for (std::size_t i = 0; i < keyLen; ++i) {
                    key += characters[randomIndex];
                    randomIndex = std::rand() % characters.size();
                }

                return std::pair<std::string, std::string>(key+"( ",key);}
            },

            {ConditionalTokenTypes::ARGSEP,   +[](){return std::pair<std::string, std::string>(", ","");}},
            {ConditionalTokenTypes::NODE,     +[](){return std::pair<std::string, std::string>("$ ","");}},
            {ConditionalTokenTypes::LPAREN,   +[](){return std::pair<std::string, std::string>("( ","");}},
            {ConditionalTokenTypes::RPAREN,   +[](){return std::pair<std::string, std::string>(") ","");}}
            //{ConditionalTokenTypes::STOP,     +[](){return std::pair<std::string, std::string>("","");}}
        };


        //////////////////
        //TEST GENERATOR
        //////////////////
        const std::size_t numRandomElements = 100;
        std::vector<std::tuple<ConditionalTokenTypes, std::string>> testVector;

        std::string testString;

        for (std::size_t i = 0; i < numRandomElements; ++i) {

            int randomIndex = std::rand() % LexerTestMap.size();
            // Get an iterator to the random element in the map
            auto it = std::next(LexerTestMap.begin(), randomIndex);
            // Access the random key and lambda value separately using structured binding
            ConditionalTokenTypes randomKey = it->first;

            std::function<std::pair<std::string, std::string>()> randomValue = it->second;
            std::pair<std::string, std::string> result = randomValue();
        
            testString += result.first;
            testVector.emplace_back(randomKey, result.second);

       
        }

        ConditionalLexer conditionalLexer = ConditionalLexer(testString);

        for (std::tuple<ConditionalTokenTypes, std::string> testToken : testVector) {
            ConditionalTokenTypes tokenToFind = std::get<0>(testToken);
            std::string lexemToFind = std::get<1>(testToken);
            std::shared_ptr<ParsingToken<ConditionalTokenTypes>> token = conditionalLexer.getNextToken();


            std::ostringstream errorMessage;
            errorMessage << "\n we whant :"<< lexemToFind << "\n we get : "<< token->getLexeme() <<"\n"<< "on \n" << testString << " :\n "  ;

            CAPTURE(errorMessage.str());
            REQUIRE(token->getLexeme() == lexemToFind);
            REQUIRE(token->getType() == tokenToFind);
        }
        std::shared_ptr<ParsingToken<ConditionalTokenTypes>> token = conditionalLexer.getNextToken();
        REQUIRE(token->getType() == ConditionalTokenTypes::STOP);
    }


}