/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <string>
#include <vector>
#include <array>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Transpose.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void declare_Transpose(py::module &m) {
  const std::string pyClassName("TransposeOp");
  py::class_<Transpose_Op, std::shared_ptr<Transpose_Op>, Attributes, OperatorTensor>(
    m, "TransposeOp", py::multiple_inheritance())
  .def("get_inputs_name", &Transpose_Op::getInputsName)
  .def("get_outputs_name", &Transpose_Op::getOutputsName)
  .def("attributes_name", &Transpose_Op::staticGetAttrsName);
  declare_registrable<Transpose_Op>(m, pyClassName);
  m.def("Transpose", &Transpose, py::arg("output_dims_order"), py::arg("name") = "");
}

void init_Transpose(py::module &m) {
  declare_Transpose(m);

}
} // namespace Aidge
