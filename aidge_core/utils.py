def template_docstring(template_keyword, text_to_replace):
    """Method to template docstring

    :param template: Template keyword to replace, in the documentation you template word must be between `{` `}`
    :type template: str
    :param text_to_replace: Text to replace your template with.
    :type text_to_replace: str
    """
    def dec(func):
        if "{"+template_keyword+"}" not in func.__doc__:
            raise RuntimeError(
                f"The function {function.__name__} docstring does not contain the template keyword: {template_keyword}.")
        func.__doc__ = func.__doc__.replace(
            "{"+template_keyword+"}", text_to_replace)
        return func
    return dec
