/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
TEST_CASE("[core/operator] ConvDepthWise_Op(computeReceptiveField)", "[Operator][computeReceptiveFiled][ConvDepthWise]") {
    auto dataProvider = Producer({16, 3, 224, 224}, "dataProvider");
    auto cdw1 = ConvDepthWise(3, {5, 5}, "cdw1");         // output dims: {16, 3, 220, 220}
    auto cdw2 = ConvDepthWise(3, {3, 3}, "cdw2");         // output dims: {16, 3, 218, 218}
    auto cdw3 = ConvDepthWise(3, {2, 2}, "cdw3", {2,2});  // output dims: {16, 3, 109, 109}
    auto cdw4 = ConvDepthWise(3, {1, 1}, "cdw4");         // output dims: {16, 3, 109, 109}

    auto g = std::make_shared<GraphView>("TestGraph");

    dataProvider->addChild(cdw1, 0);
    g->add(cdw1);
    g->addChild(cdw2, cdw1, 0);
    g->addChild(cdw3, cdw2, 0);
    g->addChild(cdw4, cdw3, 0);

    g->forwardDims();

    auto op1 = std::dynamic_pointer_cast<OperatorTensor>(cdw1 -> getOperator());
    auto op2 = std::dynamic_pointer_cast<OperatorTensor>(cdw2 -> getOperator());
    auto op3 = std::dynamic_pointer_cast<OperatorTensor>(cdw3 -> getOperator());
    auto op4 = std::dynamic_pointer_cast<OperatorTensor>(cdw4 -> getOperator());

    SECTION("Check individual receptive fields") {
        auto res1 = op1->computeReceptiveField({0,0,0,0}, {16,3,10,10});
        auto res2 = op2->computeReceptiveField({3,1,100,28}, {4,2,30,40});
        auto res3 = op3->computeReceptiveField({0,0,0,0}, {1,1,109,109});
        auto res4 = op4->computeReceptiveField({5,0,108,108}, {10,1,1,1});

        REQUIRE(((res1[0].first == std::vector<DimSize_t>({0,0,0,0})) && (res1[0].second == std::vector<DimSize_t>({16, 3, 14, 14}))));
        REQUIRE(((res2[0].first == std::vector<DimSize_t>({3,1,100,28})) && (res2[0].second == std::vector<DimSize_t>({4, 2, 32, 42}))));
        REQUIRE(((res3[0].first == std::vector<DimSize_t>({0,0,0,0})) && (res3[0].second == std::vector<DimSize_t>({1, 1, 218, 218}))));
        REQUIRE(((res4[0].first == std::vector<DimSize_t>({5,0,108,108})) && (res4[0].second == std::vector<DimSize_t>({10, 1, 1, 1}))));
    }

    SECTION("Check receptive field propagation") {
        // input:  first-{5, 0, 50, 50}  dims-{1, 1, 1, 1}
        auto res4 = op4->computeReceptiveField({5,0,50,50}, {1,1,1,1});
        // cdw4 RF:  first-{5, 0, 50, 50}  dims-{1, 1, 1, 1}
        auto res3 = op3->computeReceptiveField(res4[0].first, res4[0].second);
        // cdw3 RF:  first-{5, 0, 100, 100} dims-{1, 1, 2, 2}
        auto res2 = op2->computeReceptiveField(res3[0].first, res3[0].second);
        // cdw2 RF:  first-{5, 0, 100, 100} dims-{1, 1, 4, 4}
        auto res1 = op1->computeReceptiveField(res2[0].first, res2[0].second);
        // cdw1 RF:  first-{5, 0, 100, 100} dims-{1, 1, 8, 8}

        REQUIRE(((res1[0].first == std::vector<DimSize_t>({5, 0, 100, 100})) && (res1[0].second == std::vector<DimSize_t>({1, 1, 8, 8}))));
    }
}
}  // namespace Aidge