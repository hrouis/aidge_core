#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "aidge/nodeTester/ConditionalInterpreter.hpp"

#include "aidge/graphRegex/matchFsm/FsmNode.hpp"
#include "aidge/graphRegex/matchFsm/FsmEdge.hpp"
#include "aidge/graphRegex/matchFsm/FsmGraph.hpp"
#include "aidge/graphRegex/matchFsm/FsmRunTimeContext.hpp"

using namespace Aidge;

TEST_CASE("matchFSM", "FsmEdge") {

   
    std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(true,false);
    std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,true);
    std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("A","true==true");
    FsmEdgeUnique EdgeToTest(nodeA,nodeB,toTest);

    SECTION("FsmEdgeUnique constructor") {
        REQUIRE(EdgeToTest.getSourceNode() == nodeA);
        REQUIRE(EdgeToTest.getDestNode() == nodeB);
        REQUIRE(EdgeToTest.isCommon() == false);
    }
    
    SECTION("FsmEdgeCommon constructor") {
        std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(true,false);
        std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,true);
        std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("A","true==true");

        FsmEdgeCommon EdgeToTest(nodeA,nodeB,toTest,"A");

        REQUIRE(EdgeToTest.getSourceNode() == nodeA);
        REQUIRE(EdgeToTest.getDestNode() == nodeB);
        REQUIRE(EdgeToTest.isCommon() == true);
    }

    SECTION("FsmEdgeRef constructor") {
        std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(true,false);
        std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,true);
        std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("A","true==true");

        FsmEdgeRef EdgeToTest(nodeA,nodeB,0,-1);

        REQUIRE(EdgeToTest.getSourceNode() == nodeA);
        REQUIRE(EdgeToTest.getDestNode() == nodeB);
        REQUIRE(EdgeToTest.isCommon() == false);
    }
          
    SECTION("FsmEdgeEmpty constructor") {
        std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(true,false);
        std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,true);
        std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("A","true==true");

        FsmEdgeEmpty EdgeToTest(nodeA,nodeB);

        REQUIRE(EdgeToTest.getSourceNode() == nodeA);
        REQUIRE(EdgeToTest.getDestNode() == nodeB);
        REQUIRE(EdgeToTest.isCommon() == false);
    }


    SECTION("FsmEdgeFactory"){

    std::map<std::string, std::shared_ptr<ConditionalInterpreter>> allTest = {
        {"A",std::make_shared<ConditionalInterpreter>("A","true==true")},
        {"B",std::make_shared<ConditionalInterpreter>("B","true==true")},
        {"C",std::make_shared<ConditionalInterpreter>("C","true==true")}
    };

// make(std::shared_ptr<FsmNode> source, std::shared_ptr<FsmNode> dest, 
//     FsmEdgeTypes type,std::map<std::string, const std::shared_ptr<ConditionalInterpreter>> allTest,
//     const std::string& lexeme = "");

        std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(false,true);
        std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(true,false);
//     EMPTY = 0,
//     REF,
//     COMMON,
//     UNIQUE

        std::shared_ptr<FsmEdge> edgeE = FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::EMPTY,allTest,"");
        std::shared_ptr<FsmEdge> edgeU = FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::UNIQUE,allTest,"A");
        std::shared_ptr<FsmEdge> edgeC = FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::COMMON,allTest,"A#");
        std::shared_ptr<FsmEdge> edgeR = FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::REF,allTest,"(0,1)");

        //test detection of bad syntax lexem
        REQUIRE_THROWS(FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::EMPTY,allTest,"A"));
        REQUIRE_THROWS(FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::UNIQUE,allTest,"A#"));
        REQUIRE_THROWS(FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::COMMON,allTest,"A"));
        REQUIRE_THROWS(FsmEdgeFactory::make(nodeA,nodeB,FsmEdgeTypes::REF,allTest,"A"));

        REQUIRE(edgeE->getSourceNode() == nodeA);
        REQUIRE(edgeE->getDestNode() == nodeB);
    }

    SECTION("graph constructor") {
        //make the nodes 
        std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(true,false);
        std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,false);
        std::shared_ptr<FsmNode>  nodeC = std::make_shared<FsmNode>(false,true);

        //make the edges
        std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("A","true==true");
        std::shared_ptr<FsmEdge> edgeAB =  std::make_shared<FsmEdgeUnique>(nodeA,nodeB,toTest);
        std::shared_ptr<FsmEdge> edgeBC =  std::make_shared<FsmEdgeUnique>(nodeB,nodeC,toTest);
 
        std::shared_ptr<FsmGraph> graph =  std::make_shared<FsmGraph>("");

        graph->addEdge(edgeAB);
        graph->addEdge(edgeBC);
        

        REQUIRE(graph->getValidNodes() == std::set<std::shared_ptr<FsmNode>>{nodeA});
        REQUIRE(graph->getStartNodes() == std::vector<std::shared_ptr<FsmNode>>{nodeC});
    }


    SECTION("graph merge") {

        std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("A","true==true");

        //make the nodes 
        std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(false,true);
        std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,false);
        std::shared_ptr<FsmNode>  nodeC = std::make_shared<FsmNode>(true,false);

        //make the edges
        
        std::shared_ptr<FsmEdge> edgeAB =  std::make_shared<FsmEdgeUnique>(nodeA,nodeB,toTest);
        std::shared_ptr<FsmEdge> edgeBC =  std::make_shared<FsmEdgeUnique>(nodeB,nodeC,toTest);
 
        std::shared_ptr<FsmGraph> graph =  std::make_shared<FsmGraph>("");
        graph->addEdge(edgeAB);
        graph->addEdge(edgeBC);

        REQUIRE(graph->getValidNodes() == std::set<std::shared_ptr<FsmNode>>{nodeC});
        REQUIRE(graph->getStartNodes() == std::vector<std::shared_ptr<FsmNode>>{nodeA});
        REQUIRE(graph->getNodes() == std::set<std::shared_ptr<FsmNode>>{nodeA,nodeB,nodeC});

                //make the nodes 
        std::shared_ptr<FsmNode>  node2A = std::make_shared<FsmNode>(false,true);
        std::shared_ptr<FsmNode>  node2B = std::make_shared<FsmNode>(false,false);
        std::shared_ptr<FsmNode>  node2C = std::make_shared<FsmNode>(true,false);


        std::shared_ptr<FsmEdge> edge2AB =  std::make_shared<FsmEdgeUnique>(node2A,node2B,toTest);
        std::shared_ptr<FsmEdge> edge2BC =  std::make_shared<FsmEdgeUnique>(node2B,node2C,toTest);
 
        std::shared_ptr<FsmGraph> graph2 =  std::make_shared<FsmGraph>("");


        graph2->addEdge(edge2AB);
        graph2->addEdge(edge2BC);

        
        REQUIRE(graph2->getValidNodes() == std::set<std::shared_ptr<FsmNode>>{node2C});
        REQUIRE(graph2->getStartNodes() == std::vector<std::shared_ptr<FsmNode>>{node2A});
        REQUIRE(graph2->getNodes() == std::set<std::shared_ptr<FsmNode>>{node2A,node2B,node2C});


        graph->mergeOneStartOneValid(graph2);

        REQUIRE(graph->getValidNodes() == std::set<std::shared_ptr<FsmNode>>{node2C});
        REQUIRE(graph->getStartNodes() == std::vector<std::shared_ptr<FsmNode>>{nodeA});
        REQUIRE(graph->getNodes() == std::set<std::shared_ptr<FsmNode>>{nodeA,nodeB,nodeC,node2B,node2C});
    }




}

// TEST_CASE("matchFSM", "FsmGraph") {

//     SECTION("FsmEdgeUnique constructor") {
//         //make the nodes 
//         std::shared_ptr<FsmNode>  nodeA = std::make_shared<FsmNode>(true,false);
//         std::shared_ptr<FsmNode>  nodeB = std::make_shared<FsmNode>(false,true);

//         //make the edges
//         std::shared_ptr<ConditionalInterpreter> toTest =  std::make_shared<ConditionalInterpreter>("true==true");
//         std::shared_ptr<FsmEdgeUnique> edge =  std::make_shared<FsmEdgeUnique>(nodeA,nodeB,toTest);
 
//         std::shared_ptr<FsmGraph> graph =  std::make_shared<FsmGraph>("");

//         graph->addEdge(edge);
        


//     }

// }